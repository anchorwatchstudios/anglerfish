var SOLAR_MASS = 1989000000000000000000000000000;
var LIGHT_YEAR = 9461000000000;

var PARTICLES = 750;
var MIN_PARTICLE_MASS = SOLAR_MASS / 100;
var MAX_PARTICLE_MASS = SOLAR_MASS * 100;
var PARTICLE_SPEED = 3;
var GALAXIES = 5;
var GALAXY_VARIANCE = 50;
var GREAT_ATTRACTOR_MASS = SOLAR_MASS * 1000000000000000;
var DRAW_GREAT_ATTRACTOR = true;
var SCALE = LIGHT_YEAR * 100;

var canvas;
var universe;

function setup() {
	canvas = createCanvas(windowWidth, windowHeight);
	ellipseMode(RADIUS);
	universe = new Universe(PARTICLES, MIN_PARTICLE_MASS, MAX_PARTICLE_MASS, PARTICLE_SPEED, GALAXIES, GALAXY_VARIANCE, GREAT_ATTRACTOR_MASS, DRAW_GREAT_ATTRACTOR, SCALE);
	universe.init(canvas.drawingContext);
}

function draw() {
	background("#000");
	universe.draw();
}

function windowResized() {
	resizeCanvas(windowWidth, windowHeight);
	// TODO: Center canvas
}

function keyPressed() {
	if(key == ' ') {
		universe.init(canvas.drawingContext);
	}
}