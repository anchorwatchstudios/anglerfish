# AnglerFish

AnglerFish is a galaxy evolution simulator. It illustrates the way galaxies interact with each other and change over time. Press the space bar or reload the page to reset the simulation.

I created this small project after seeing a few videos by The Coding Train, a p5js evangelist. One of his videos was a coding challenge for creating purple rain in honor of the late, great, musician Prince. That video intrigued me and got me interested in learning more about p5js. I highly recommend his videos.

I thought it would be interesting to try to rapidly prototype orbit, so I gave it a shot! I didn’t do any realistic modeling with advanced physics, but I tried to capture the importance of mass, distance, and angular momentum. On each update the particles sum the forces exerted on them by all other nearby particles, then the sum of those forces are applied.

## Demo
[ anchorwatchstudios.com/demo/anglerfish/ ](https://anchorwatchstudios.com/demo/anglerfish/)