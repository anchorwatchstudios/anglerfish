/**
 * Represents the universe.
 *
 * @param n The number of particles
 * @param minMass The minimum mass of a particle
 * @param maxMass The maximum mass of a particle
 * @param sv The maximum start velocity of a particle
 * @param g The number of galaxies
 * @param v The maximum variance of galaxies
 * @param am The great attractor mass
 * @param am Draw attractor?
 * @param s The scale in km per pixel
 */
function Universe(n, minMass, maxMass, sv, g, v, am, ad, s) {
	
	this.n = n;
	this.g = g;
	this.minMass = minMass;
	this.maxMass = maxMass;
	this.am = am;
	this.sv = sv;
	this.v = v;
	this.s = s;
	this.ad = ad;
	var particles;
	
	/**
	 * Updates and draws all the particles in the universe.
	 */
	this.draw = function() {
		for(var i = 0;i < particles.length;i++) {
			particles[i].update(particles);
			particles[i].draw();
		}
	}

	/**
	 * Initializes the universe with the given drawing context.
	 * 
	 * @param context The canvas drawing context
	 */
	this.init = function(context) {
		particles = [];
		initBlackHole(context, new Vec2(width / 2, height / 2), 25, new Vec2(0, 0), this.am);
		for(var i = 0;i < this.g;i++) {
			var x = Math.floor(Math.random() * width);
			var y = Math.floor(Math.random() * height);
			var n1 = y < height / 2 ? 1 : -1;
			var n2 = x < width / 2 ? -1 : 1;
			var v = new Vec2(n1 * this.sv, n2 * this.sv);
			initGalaxy(
				context,
				x,
				y,
				v,
				Math.random() * this.v,
				this.n / this.g
			);
		}
	}

	function initBlackHole(context, position, radius, velocity, attractorMass) {
		var blackHole = new Particle(
			position,
			radius,
			velocity,
			attractorMass,
			s,
			"#33333300"
		);
		blackHole.draw = function() {
			if(ad) {
				var gradient = context.createRadialGradient(
					blackHole.p.x, 
					blackHole.p.y, 
					blackHole.r, 
					blackHole.p.x + blackHole.r, 
					blackHole.p.y + blackHole.r, 
					blackHole.r
				);
				gradient.addColorStop(0, "black");
				gradient.addColorStop(1, blackHole.c);
				context.fillStyle = gradient;
				ellipse(blackHole.p.x, blackHole.p.y, blackHole.r, blackHole.r);
			}
		}
		particles.push(blackHole);
	}

	function initGalaxy(context, x, y, v, variance, size) {
		//initBlackHole(context, new Vec2(x, y), 15, v, am / 100);
		for(var i = 0;i < size;i++) {
			var n1 = Math.random() < 0.5 ? -1 : 1;
			var n2 = Math.random() < 0.5 ? -1 : 1;
			x += Math.floor(Math.random() * variance) * n1;
			y += Math.floor(Math.random() * variance) * n2;
			var m = minMass + Math.random() * (maxMass - minMass);
			particles.push(
				new Particle(
					new Vec2(x, y),
					3,// + (m / (SOLAR_MASS * 10)), 
					new Vec2(v.x, v.y),
					m,
					s,
					randomColor()
				)
			);
		}
	}
	
	function colorByMass(mass) {
		var percent = mass / maxMass;
		var red;
		var green;
		var blue;
		if(percent < 0.33) {
			red = "FF";
			green = "00";
			blue = "00";
		}
		else if(percent < 0.66) {
			red = "FF";
			green = "FF";
			blue = "00";
		}
		else if(percent < 0.75) {
			red = "FF";
			green = "FF";
			blue = "FF";
		}
		else {
			red = "30";
			green = "30";
			blue = "FF";
		}
		return "#" + red + green + blue;
	}
	
	function randomColor() {
		var c = "#" + Math.floor(Math.random() * 0xFF).toString(16) + "13" + Math.floor(Math.random() * 0xFF).toString(16);
		while(c.length < 7) {
			c += "F";
		}
		return c;
	}

}