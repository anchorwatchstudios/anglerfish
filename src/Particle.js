/**
 * Represents a particle in space.
 *
 * @param p A Vec2 representing position
 * @param r The radius
 * @param v A Vec2 representing velocity
 * @param m The mass
 * @param s The scale in km per pixel
 * @param c The color
 */
function Particle(p, r, v, m, s, c) {
	
	this.r = r;
	this.p = p;
	this.v = v;
	this.m = m;
	this.c = c;
	this.s = s;
	var G = 0.00000000000667408;
	
	/**
	 * Updates the particle by summing the
	 * gravitational forces of the given
	 * particles and applying the force.
	 *
	 * @param particles An array of particles
	 */
	this.update = function(particles) {
		// TODO: Optimize to n^2/2 by doing both particles with one distance
		for(var i = 0;i < particles.length;i++) {
			var distance = this.p.getDistance(particles[i].p);
			if(distance != 0) {
				distance *= this.s;
				var force = G * this.m * particles[i].m / Math.pow(distance, 2);
				var accel = force / this.m;
				var direction = this.p.getDirection(particles[i].p);
				this.v.x += accel * direction.x;
				this.v.y += accel * direction.y;
			}
		}
		this.p.x += this.v.x;
		this.p.y += this.v.y;
	}
	
	/**
	 * Draws the particle.
	 */
	this.draw = function() {
		fill(this.c);
		ellipse(this.p.x, this.p.y, this.r, this.r);
	}
	
}