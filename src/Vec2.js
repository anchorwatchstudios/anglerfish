/**
 * Represents a two-dimensional vector.
 *
 * @param x The x component of this vector
 * @param y The y component of this vector
 */
function Vec2(x, y) {
	
	this.x = x;
	this.y = y;
	
	/**
	 * Returns the distance from this point to the
	 * given destination.
	 *
	 * @param destination The destination
	 * @return The distance between the two points
	 */
	this.getDistance = function(destination) {
		var xDiff = this.x - destination.x;
		var yDiff = this.y - destination.y;
		return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
	}
	
	/**
	 * Returns the direction from this point to the
	 * given destination.
	 *
	 * @param destination The destination
	 * @return The direction from this point to the given point
	 */
	this.getDirection = function(destination) {
		return new Vec2(destination.x - this.x, destination.y - this.y).normalize();
	}
	
	/**
	 * Normalizes this vector.
	 *
	 * @return This vector
	 */
	this.normalize = function() {
		var length = this.getDistance(new Vec2(0, 0));
		if(length == 0) {
			return this;
		}
		this.x /= length;
		this.y /= length;
		return this;
	}
	
}